<?php
/**
 * Created by PhpStorm.
 * User: nuonchanndarong
 * Date: 3/16/16
 * Time: 12:03 PM
 */
class User extends CI_Model
{
    public $id;
    public $username;
    public $password;
    public $role;

    public function getAll()
    {
        $sql = "SELECT * FROM user";

        $query = $this->db->query($sql);

        if($query->num_rows() > 0)
        {
            $users = array();
            foreach($query->result() as $row)
            {
                $u = new User();
                $u->id = $row->id;
                $u->username = $row->username;
                $u->password = $row->password;
                $u->role = $row->role;

                $users[] = $u;
            }
            return $users;
        }
        return null;
    }

    public function getById($id)
    {
        $sql = "SELECT * FROM user WHERE id=?";

        $param = array($id);

        $query = $this->db->query($sql, $param);

        if($query->num_rows() > 0)
        {
            foreach($query->result() as $row)
            {
                $u = new User();
                $u->id = $row->id;
                $u->username = $row->username;
                $u->password = $row->password;
                $u->role = $row->role;

                return $u;
            }
        }
        return null;
    }

    public function login($username, $password)
    {
        $sql = "SELECT * FROM user WHERE username = ? AND password = ?";

        $param = array($username, $password);

        $query = $this->db->query($sql, $param);

        if($query->num_rows() > 0)
        {
            foreach($query->result() as $row)
            {
                $u = new User();
                $u->id = $row->id;
                $u->username = $row->username;
                $u->password = $row->password;
                $u->role = $row->role;

                return $u;
            }
        }
        return null;
    }
}