<?php
/**
 * Created by PhpStorm.
 * User: nuonchanndarong
 * Date: 3/16/16
 * Time: 12:16 PM
 */
class Account extends CI_Controller
{

    public function index()
    {
        $sess_username = $this->session->userdata('username');
        if(!isset($sess_username))
        {
            redirect(base_url("account/login"));
        }

        echo "Account page";
        echo "<a href='".base_url("account/logout")."'>Logout</a>";
    }

    public function login()
    {
        $sess_username = $this->session->userdata('username');
        if(isset($sess_username))
        {
            redirect(base_url("account"));
        }

        //$username = $_SESSION['username'];
        $submit = $this->input->post('submit');
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        if(isset($submit))
        {
            $currentUser = $this->user->login($username, $password);

            if($currentUser != null)
            {
                //$_SESSION['username'] = $username;
                $this->session->set_userdata("userid",$currentUser->id);
                $this->session->set_userdata("username",$username);
                redirect(base_url("account"));
            }else{
                $message = "Incorrect username or/and password!";
                $data = array(
                    "message" => $message
                );

                $this->load->view("login_view",$data);
            }

        }else{
            $this->load->view("login_view");
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url("account/login"));
    }

}